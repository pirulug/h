<!DOCTYPE html>
<html lang="es" class=" scrollbar-type-1 sb-cyan">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Metro 4 -->
    <link rel="stylesheet" href="vistas/assets/vendors/metro4/css/metro-all.min.css">
    <link rel="stylesheet" href="vistas/assets/css/index.css">

    <title>Gueulaes</title>

    <script>
        window.on_page_functions = [];
    </script>
</head>
<body class="m4-cloak h-vh-100">

<div data-role="navview" data-toggle="#paneToggle" data-expand="xl" data-compact="lg" data-active-state="true">

    <!-- Menu Sidevar -->
    <?php include 'vistas/modulos/menu-sidevar.php' ?>
    <!-- Menu Sidevar -->

    <div class="navview-content h-100">

        <!-- Top Menu -->
        <?php include 'vistas/modulos/menu-top.php' ?>
        <!-- Top Menu -->

        <!-- Contenido -->
        <div class="content-inner h-100" style="overflow-y: auto">
            <?php include 'vistas/modulos/admin/dashboard.php' ?>
        </div>
        <!-- Contenido -->

    </div>
</div>


<!-- jQuery first, then Metro UI JS -->
<script src="vistas/assets/vendors/jquery/jquery-3.4.1.min.js"></script>
<script src="vistas/assets/vendors/chartjs/Chart.bundle.min.js"></script>
<script src="vistas/assets/vendors/qrcode/qrcode.min.js"></script>
<script src="vistas/assets/vendors/jsbarcode/JsBarcode.all.min.js"></script>
<script src="vistas/assets/vendors/ckeditor/ckeditor.js"></script>
<script src="vistas/assets/vendors/metro4/js/metro.min.js"></script>

<script src="vistas/assets/js/index.js"></script>

<script src="vistas/assets/js/charts.js"></script>
</body>
</html>