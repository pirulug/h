<div class="navview-pane">
    <div class="bg-cyan d-flex flex-align-center">
        <button class="pull-button m-0 bg-darkCyan-hover">
            <span class="mif-menu fg-white"></span>
        </button>
        <h2 class="text-light m-0 fg-white pl-7" style="line-height: 52px">Gueulaes</h2>
    </div>

    <div class="suggest-box">
        <div class="data-box">
            <img src="vistas/assets/images/jek_vorobey.jpg" class="avatar">
            <div class="ml-4 avatar-title flex-column">
                <a href="#" class="d-block fg-white text-medium no-decor"><span class="reduce-1">Jack Sparrow</span></a>
                <p class="m-0"><span class="fg-green mr-2">&#x25cf;</span><span class="text-small">online</span></p>
            </div>
        </div>
        <img src="vistas/assets/images/jek_vorobey.jpg" class="avatar holder ml-2">
    </div>

    <div class="suggest-box">
        <input type="text" data-role="input" data-clear-button="false" data-search-button="true" placeholder="Buscar..">
        <button class="holder">
            <span class="mif-search fg-white"></span>
        </button>
    </div>

    <ul class="navview-menu mt-4" id="side-menu">
        <li class="item-header">Menu de Navegacion</li>
        <li>
            <a href="dashboard">
                <span class="icon"><span class="mif-meter"></span></span>
                <span class="caption">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><span class="mif-widgets"></span></span>
                <span class="caption">Widgets</span>
            </a>
        </li>

        <li>
            <a href="#" class="dropdown-toggle">
                <span class="icon"><span class="mif-magic-wand"></span></span>
                <span class="caption">Wizards</span>
            </a>
            <ul class="navview-menu stay-open" data-role="dropdown" >
                <li class="item-header">Wizards</li>
                <li>
                    <a href="#">
                        <span class="icon"><span class="mif-spinner2"></span></span>
                        <span class="caption">Master</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon"><span class="mif-spinner2"></span></span>
                        <span class="caption">Wizard</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="item-header">Documentation</li>
        <li>
            <a href="https://metroui.org.ua/intro.html">
                <span class="icon"><span class="mif-brightness-auto fg-red"></span></span>
                <span class="caption">Metro 4</span>
            </a>
        </li>
    </ul>

    <div class="w-100 text-center text-small data-box p-2 border-top bd-grayMouse" style="position: absolute; bottom: 0">
        <div>
            &copy; 2020 
            <a href="http://gueulaes.net" class="text-muted fg-white-hover no-decor">Gueulaes.net</a>        
        </div>
        <div>
            Created with 
            <a href="https://twitter.com/pirulug" class="text-muted fg-white-hover no-decor">Pirulug</a>
        </div>
    </div>
</div>