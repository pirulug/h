<div data-role="appbar" class="pos-absolute bg-darkCyan fg-white">

    <a href="#" class="app-bar-item d-block d-none-lg" id="paneToggle"><span class="mif-menu"></span></a>

    <div class="app-bar-container ml-auto">
        
        <div class="app-bar-container">
            <a href="#" class="app-bar-item">
                <img src="vistas/assets/images/jek_vorobey.jpg" class="avatar">
                <span class="ml-2 app-bar-name">Jack Sparrow</span>
            </a>
            <div class="user-block shadow-1" data-role="collapse" data-collapsed="true">
                <div class="bg-darkCyan fg-white p-2 text-center">
                    <img src="vistas/assets/images/jek_vorobey.jpg" class="avatar">
                    <div class="h4 mb-0">Jack Sparrow</div>
                    <div>Pirate captain</div>
                </div>
                <div class="bg-white d-flex flex-justify-between flex-equal-items p-2">
                    <button class="button flat-button">Configuracion</button>
                    <button class="button flat-button">Sales</button>
                    <button class="button flat-button">Friends</button>
                </div>
                <div class="bg-white d-flex flex-justify-between flex-equal-items p-2 bg-light">
                    <button class="button mr-1">Perfil</button>
                    <button class="button ml-1">Serrar Sesion</button>
                </div>
            </div>
        </div>
        <a href="#" class="app-bar-item">
            <span class="mif-cogs"></span>
        </a>
    </div>
</div>