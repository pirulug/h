<?php

require_once "conexion.php";

class ModeloMirror{

    //crear mirror
    static public function mdlIngresarMirror($tabla,$datos){
        
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(titulo, servidor1, servidor2, servidor3, parte1, parte2, parte3, enlace1, enlace2, enlace3, corto1, corto2, corto3) VALUES (:titulo, :servidor1, :servidor2, :servidor3, :parte1, :parte2, :parte3, :enlace1, :enlace2, :enlace3, :corto1, :corto2, :corto3)");
        
        $stmt->bindParam(":titulo", $datos["titulo"], PDO::PARAM_STR);
        $stmt->bindParam(":servidor1", $datos["servidor1"], PDO::PARAM_STR);
        $stmt->bindParam(":servidor2", $datos["servidor2"], PDO::PARAM_STR);
        $stmt->bindParam(":servidor3", $datos["servidor3"], PDO::PARAM_STR);
        $stmt->bindParam(":parte1", $datos["parte1"], PDO::PARAM_STR);
        $stmt->bindParam(":parte2", $datos["parte2"], PDO::PARAM_STR);
        $stmt->bindParam(":parte3", $datos["parte3"], PDO::PARAM_STR);
        $stmt->bindParam(":enlace1", $datos["enlace1"], PDO::PARAM_STR);
        $stmt->bindParam(":enlace2", $datos["enlace2"], PDO::PARAM_STR);
        $stmt->bindParam(":enlace3", $datos["enlace3"], PDO::PARAM_STR);
        $stmt->bindParam(":corto1", $datos["corto1"], PDO::PARAM_STR);
        $stmt->bindParam(":corto2", $datos["corto2"], PDO::PARAM_STR);
        $stmt->bindParam(":corto3", $datos["corto3"], PDO::PARAM_STR);
        
		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
		$stmt = null;
    }

    //mostrar mirror
    static public function mdlMostrarMirror($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		

		$stmt -> close();

		$stmt = null;

	}
	
	//EDITAR CATEGORIA

	static public function mdlEditarCategoria($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET categoria = :categoria WHERE id = :id");

		$stmt -> bindParam(":categoria", $datos["categoria"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}
    
    //Borrar Mirror
    static public function mdlBorrarMirror($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;


	}

}