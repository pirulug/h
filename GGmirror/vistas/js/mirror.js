/*=============================================
EDITAR CATEGORIA
=============================================*/
$(".table").on("click", ".btnEditarMirror", function () {
  var idMirror = $(this).attr("idMirror");

  var datos = new FormData();
  datos.append("idMirror", idMirror);

  $.ajax({
    url: "ajax/mirror.ajax.php",
    method: "POST",
    data: datos,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "json",
    success: function (respuesta) {
      $("#editarTitulo").val(respuesta["1"]);

      $("#editarServidor1").val(respuesta["2"]);
      $("#editarServidor2").val(respuesta["3"]);
      $("#editarServidor3").val(respuesta["4"]);

      $("#editarServidor1").html(respuesta["2"]);
      $("#editarServidor2").html(respuesta["3"]);
      $("#editarServidor3").html(respuesta["4"]);

      $("#editarParte1").val(respuesta["5"]);
      $("#editarParte2").val(respuesta["4"]);
      $("#editarParte3").val(respuesta["7"]);

      $("#editarParte1").html(respuesta["5"]);
      $("#editarParte2").html(respuesta["4"]);
      $("#editarParte3").html(respuesta["7"]);

      $("#editarEnlace1").val(respuesta["8"]);
      $("#editarEnlace2").val(respuesta["9"]);
      $("#editarEnlace3").val(respuesta["10"]);
      console.log(respuesta);
    },
  });
});

/*=============================================
ELIMINAR Mirror
=============================================*/
$(".table").on("click", ".btnBorrarMirror", function () {
  var idMirror = $(this).attr("idMirror");

  swal({
    title: "¿Está seguro de borrar el usuario?",
    text: "¡Si no lo está puede cancelar la accíón!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    cancelButtonText: "Cancelar",
    confirmButtonText: "Si, borrar usuario!",
  }).then(function (result) {
    if (result.value) {
      window.location = "index.php?ruta=inicio&idMirror=" + idMirror;
    }
  });
});
