<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="inicio">GGmirror</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="ayuda">AYUDA <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevo">NUEVO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">Iniciar Secion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="salir">Cerrar Secion</a>
                </li>
            </ul>
        </div>
    </div>
</nav>