<!-- Plugins CSS -->
<!-- Bootstrap -->
<link rel="stylesheet" href="vistas/css/flat.css">
<!-- Fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">


<!-- Plugins JS -->
<!-- Jquery -->
<script src="vistas/plugins/jquery/dist/jquery.min.js"></script>
<!-- SwitAlet2 -->
<script src="vistas/plugins/sweetalert2/sweetalert2.all.js"></script>
<!-- Bootstrao js -->
<script src="vistas/plugins/bootstrap/js/bootstrap.js"></script>
<script src="vistas/plugins/popper/popper.min.js"></script>