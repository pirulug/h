<!DOCTYPE html>
<html lang="es-pe">
<head>
    <meta charset="UTF-8">
    <title>GGmirror</title>

<?php include "partes/links.php"?>   

</head>

<body>

     <?php

      //if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){

        include 'partes/navbar.php';
          
        if(isset($_GET["ruta"])){

          if($_GET["ruta"] == "nuevo" ||
             $_GET["ruta"] == "login" ||
             $_GET["ruta"] == "inicio" ||
             $_GET["ruta"] == "ver" ||
             $_GET["ruta"] == "editar" ||
             $_GET["ruta"] == "salir"){

            include "content/".$_GET["ruta"].".php";

          }else{

            include "error/404.php";

          }

        }else{

          include "content/inicio.php";

        }

        include "partes/footer.php";

      //}else{

        //include "partes/login.php";

      //}

      ?>
    
    
<script src="vistas/js/mirror.js"></script>
    
</body>
</html>