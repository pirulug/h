<?php

$mirrorID = $_GET['v'];

$item = 'id';
$valor = $mirrorID;

$mirror = ControladorMirror::ctrMostrarMirror($item, $valor);

$fila = mysql_fetch_row($mirror);

?>
    
    <div class="container jumbotron my-3 p-5">
        <div class="card">
            <div class="card-body text-center">
               <h6>Has solicitado el siguiente archivo</h6>
               <h3><?= $fila['1'] ?></h3>
                <p>
                    <b>Tamaño del archivo :</b> 5.58 MB<br>
                    <b>Fecha de subida :</b> 09 Oct, 2020<br>
                    <b>Número de vistas :</b> 28<br>
                    <b>Tipo de archivo :</b> Archive
                </p>
            </div>
        </div>
        
        <div class="card text-center mt-3 p-3 bg-success h5">
              ¡Escoge un enlace desde tu sitio de hosting favorito!
        </div>
        
        <div class="card mt-3">
            <div class="card-body">
                <table class="table table-hover text-center">
                    <thead>
                        <tr>
                            <th class="h5">Host</th>
                            <th class="h5">Enlace del Archivo</th>
                            <th class="h5">Estado del Enlace</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <img src="vistas/img/MegaNz.png" alt="">
                            </td>
                            <td>
                                <a href="<?= $fila['corto1'] ?>" class="btn btn-lg btn-primary"><i class="fa fa-download"></i> Obtener Enlace</a>
                            </td>
                            <td>
                                <p class="h5 text-success">Activo</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>