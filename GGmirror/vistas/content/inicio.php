    <div class="container jumbotron my-3 p-5">
        
        <div class="card mt-3">
            <div class="card-body">
                <table class="table table-hover text-center">
                    <thead>
                        <tr>
                            <th class="h5">Titulo</th>
                            <th class="h5">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                        $item = null;
                        $valor = null;

                        $mirror = ControladorMirror::ctrMostrarMirror($item, $valor);

                        foreach ($mirror as $key => $value){

                    ?>
                        <tr>
                            <td><?= $value['titulo'] ?></td>
                            <td>
                                <a href="" idMirror="<?= $value['id'] ?>" class="btn btn-outline-primary btnVerMirror" ><i class="fa fa-eye"></i> Ver</a>
                                <button class="btn btn-outline-success btnEditarMirror" idMirror="<?= $value['id'] ?>"  data-toggle="modal" data-target="#editarMirror"><i class="fa fa-edit"></i> Editar</button>
                                <button class="btn btn-outline-danger btnBorrarMirror" idMirror="<?= $value['id'] ?>"><i class="fa fa-trash-alt"></i> Eliminar</button>
                            </td>
                        </tr>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<!-- Modal editar mirror -->
<div class="modal fade" id="editarMirror" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Editar Mirrors</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form role="form" method="post">
        <div class="modal-body">
            <input class="form-control" placeholder="Titulo" id="editarTitulo" name="editarTitulo" />
            <div class="my-2">
                <h6>Server 1</h6>
            <div class="input-group mb-3">
                <select class="form-control" name="editarServidor1">
                    <option value="" id="editarServidor1"></option>
                    <option value="Mega">MEGA.NZ</option>
                    <option value="Mediafire">Mediafire</option>
                    <option value="GoogleDrive">Drive</option>
                </select>
                <select class="form-control" name="editarParte1">
                    <option value="" id="editarParte1"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <input class="form-control" type="text" id="editarEnlace1" name="editarEnlace1" placeholder="http://...">
            </div>
            <hr>
            <div class="my-2">
                <h6>Server 2</h6>
            <div class="input-group mb-3">
                <select class="form-control" name="editarServidor2">
                    <option value="" id="editarServidor2"></option>
                    <option value="Mega">MEGA.NZ</option>
                    <option value="Mediafire">Mediafire</option>
                    <option value="GoogleDrive">Drive</option>
                </select>
                <select class="form-control" name="editarParte2">
                    <option value="" id="editarParte1"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <input class="form-control" type="text" id="editarEnlace2"name="editarEnlace2" placeholder="http://...">
            </div>
            <hr>
            <div class="my-2">
                <h6>Server 3</h6>
            <div class="input-group mb-3">
                <select class="form-control" name="editarServidor3">
                    <option value="" id="editarServidor3"></option>
                    <option value="Mega">MEGA.NZ</option>
                    <option value="Mediafire">Mediafire</option>
                    <option value="GoogleDrive">Drive</option>
                </select>
                <select class="form-control" name="editarParte3">
                    <option value="" id="editarParte3"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <input class="form-control" type="text" id="editarEnlace3" name="editarEnlace3" placeholder="http://...">
            </div>            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary" name="editarMirror" class="btn btn-primary my-2">Guardar</button>
        </div>
        <?php

            $editarMirror = new ControladorMirror();
            $editarMirror -> ctrEditarMirror();

        ?>
        </form>
    </div>
  </div>
</div>


<?php
    $borrarMirror = new ControladorMirror();
    $borrarMirror -> ctrBorrarMirror();
?>

