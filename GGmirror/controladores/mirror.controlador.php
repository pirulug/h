<?php

class ControladorMirror{

  //crear mirror
  static public function ctrCrearMirror(){

    if(isset($_POST["nuevoMirror"])){

            $url_larga1 = $_POST['enlace1'];
            $url_larga2 = $_POST['enlace2'];
            $url_larga3 = $_POST['enlace3'];
            
            //ouo.io
            $url_larga1 = $url_larga1;
            $url_larga2 = $url_larga2;
            $url_larga3 = $url_larga3;

            $servicio_web = "http://ouo.io/api/";
            $key = "QJfmbuUj";
            $fn = "?s=";

            $acorta_do1 = $servicio_web.$key.$fn.$url_larga1;
            $acorta_do2 = $servicio_web.$key.$fn.$url_larga2;
            $acorta_do3 = $servicio_web.$key.$fn.$url_larga3;

            $respuesta_cut_1 = file_get_contents($acorta_do1);
            $respuesta_cut_2 = file_get_contents($acorta_do2);
            $respuesta_cut_3 = file_get_contents($acorta_do3);

            $tabla = "lista";
            $datos = array("titulo" => $_POST['titulo'],
                            "servidor1" => $_POST['servidor1'],
                            "servidor2" => $_POST['servidor2'],
                            "servidor3" => $_POST['servidor3'],
                            "parte1" => $_POST['parte1'],
                            "parte2" => $_POST['parte2'],
                            "parte3" => $_POST['parte3'],
                            "enlace1" => $_POST['enlace1'],
                            "enlace2" => $_POST['enlace2'],
                            "enlace3" => $_POST['enlace3'],
                            "corto1" => "$respuesta_cut_1",
                            "corto2" => "$respuesta_cut_2",
                            "corto3" => "$respuesta_cut_3");
                
            $respuesta = ModeloMirror::mdlIngresarMirror($tabla, $datos);
            
            if($respuesta == "ok"){

                echo'<script>
                $(document).ready(function(){
                    swal({
                        title: "Guardado!",
                        text: "Los enlaces se guardaron correctamente",
                        type: "success"
                      }).then(function() {
                        window.location = "inicio";
                      });
                    });
                </script>';

            }

    }

  }

  //editar mirror
  static public function ctrEditarMirror(){

		if(isset($_POST["editarCategoria"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarCategoria"])){

				$tabla = "categorias";

				$datos = array("categoria"=>$_POST["editarCategoria"],
							   "id"=>$_POST["idCategoria"]);

				$respuesta = ModeloCategorias::mdlEditarCategoria($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido cambiada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "categorias";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "categorias";

							}
						})

			  	</script>';

			}

		}

	}
  
  //Mostrar mirror
  static public function ctrMostrarMirror($item, $valor){

    $tabla = "lista";

    $respuesta = ModeloMirror::MdlMostrarMirror($tabla, $item, $valor);

    return $respuesta;
  }
  
  //Borrar Mirror
  static public function ctrBorrarMirror(){
    
    if(isset($_GET["idMirror"])){

      $tabla ="lista";
      $datos = $_GET["idMirror"];

      $respuesta = ModeloMirror::mdlBorrarMirror($tabla, $datos);

      if($respuesta == "ok"){

        echo'<script>

          swal({
              type: "success",
              title: "La categoría ha sido borrada correctamente",
              showConfirmButton: true,
              confirmButtonText: "Cerrar"
              }).then(function(result){
                  if (result.value) {

                  window.location = "inicio";

                  }
                })

          </script>';
      }
    }
  }



}