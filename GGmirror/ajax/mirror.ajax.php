<?php
require_once "../controladores/mirror.controlador.php";
require_once "../modelos/mirror.modelo.php";

class AjaxMirror{

	/*=============================================
	EDITAR CATEGORÍA
	=============================================*/	

	public $idMirror;

	public function ajaxEditarMirror(){

		$item = "id";
		$valor = $this->idMirror;

		$respuesta = ControladorMirror::ctrMostrarMirror($item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR CATEGORÍA
=============================================*/	
if(isset($_POST["idMirror"])){

	$Mirror = new AjaxMirror();
	$Mirror -> idMirror = $_POST["idMirror"];
	$Mirror -> ajaxEditarMirror();
}