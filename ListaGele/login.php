<?php

session_start();

include "config.php";

$sqlUsuario = "SELECT * FROM user WHERE id_user='1'";
$resultadoVisitas = mysqli_query($mysqli, $sqlUsuario);
$user = mysqli_fetch_assoc($resultadoVisitas);

$usuario = $user['usuario'];
$contra = $user['contra'];

if (isset($_POST['login']) && isset($_POST['password'])) //when form submitted
{
  if ($_POST['login'] === $usuario && $_POST['password'] === $contra)
  {
    $_SESSION['login'] = $_POST['login']; //write login to server storage
    header('Location: new.php'); //redirect to main
  }
  else
  {
    echo "<script>alert('Usuario o contraseña incorrectos');</script>";
    echo "<noscript>Usuario o contraseña incorrectos</noscript>";
  }
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</head>
<body>

  <div class="container mt-5">
    <div class="card m-auto" style="max-width: 500px">
      <div class="card-body">
        <form method="post">
          Login:<br><input class="form-control" name="login"><br>
          Password:<br><input class="form-control" name="password"><br>
          <input class="btn btn-primary" type="submit">
        </form>
      </div>
    </div>
  </div>
  
</body>
</html>