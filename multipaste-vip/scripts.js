
//tabs
$(document).ready(function()
{
	$(".tab_content").hide();
	$("ul.tabs li:first").addClass("active").show();
	$(".tab_content:first").show();

	$("ul.tabs li").click(function()
       {
		$("ul.tabs li").removeClass("active");
		$(this).addClass("active");
		$(".tab_content").hide();

		var activeTab = $(this).find("div").attr("href");
		$(activeTab).fadeIn();
		return false;
	});
});

function bb(tag, tbox, msg, def) {
	var sel, range, tagb;

    if (msg!=null){
		tagb=tag+'='+prompt(msg,def);
	} else {
		tagb=tag;
	}

	var txt = document.getElementById(tbox);
	if(document.selection) {
		txt.focus();
		sel = document.selection.createRange();
		sel.text = '[' + tagb + ']' + sel.text + '[/' + tag + ']';
	} else if(txt.selectionStart || txt.selectionStart == '0') {
		txt.value = (txt.value).substring(0, txt.selectionStart) + "["+tagb+"]" + (txt.value).substring(txt.selectionStart, txt.selectionEnd) + "[/"+tag+"]" + (txt.value).substring(txt.selectionEnd, txt.textLength);
	} else {
		txt.value = '[' + tagb + '][/' + tag + ']';
	}
}

var actBBc;

function BBhover(text){
	if (actBBc != text){
		if (actBBc != null) { document.getElementById('bbcodebutton').remove(); }
		actBBc = text;
		var en=document.getElementById(text);
		var TBC=document.getElementById('T'+text).value;
		en.innerHTML='<div id="bbcodebutton"><button onclick="bb(\'b\',\'T'+text+'\');return false;">b</button> <button onclick="bb(\'i\',\'T'+text+'\');return false;">i</button> <button onclick="bb(\'size\',\'T'+text+'\',\'Introduce el tama&ntilde;o de letra\',\'12px\');return false;">size</button> <button onclick="bb(\'color\',\'T'+text+'\',\'Introduce el color en ingles o hexadecimal\',\'red\');return false;">color</button> <button onclick="bb(\'center\',\'T'+text+'\');return false;">center</button> <button onclick="bb(\'code\',\'T'+text+'\');return false;">code</button> <button onclick="bb(\'url\',\'T'+text+'\',\'Introduce la url a enlazar\',\'http://\');return false;">URL</button> <button onclick="bb(\'img\',\'T'+text+'\');return false;">Img</button> <button onclick="bb(\'youtube\',\'T'+text+'\');return false;">YouTube</button> <button onclick="bb(\'jd\',\'T'+text+'\');return false;">JDownloader</button> <button onclick="bb(\'vip\',\'T'+text+'\');return false;" class="vipbbcodebuton">VIP</button></div>'+en.innerHTML;
		document.getElementById('T'+text).value=TBC;
	}
}

function TTab(id,text) {
	ttab = document.getElementById(id);
	if (text!=''){
		ttab.style.opacity = '1';
		ttab.innerHTML = htmlEntities(text);
	} else {
		ttab.style.opacity = '0.5';
		ttab.innerHTML = 'Desactivado';
	}
	console.log(id);
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
