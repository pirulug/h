<?php
ob_start();
$config['db_host']='localhost'; // nombre del host
$config['db_name']='paste'; // nombre de la base de datos
$config['db_user']='root'; // usuario de la BD
$config['db_pass']=''; // password de la BD

// Datos del admin y del sitio

$adminuser = 'admin'; //nombre del administrador
$adminpass = 'admin'; //password del administrador
$keylogmin = '123sdf';      //Auth Key - no dejes en blanco esto ejemplo: 2hGsTH6Fs145sWhfs36HYDAAD66
$webname = 'Multipaste'; // Nombre del sitio
$webdescription = 'Multiples Textos Compartidos'; // Descripcion del sitio

/*
Configuracion de password:
colocar "$use_password=true;" para habilitar el uso de password
y "$use_password=false;" para desabilitarlo
*/
$use_password=false;

/*
Configuracion de captcha:
colocar "$use_captcha=true;" para habilitar el uso de password
y "$use_captcha=false;" para desabilitarlo.
La Public Key y la Private Key las consigues registr�ndote en http://www.google.com/recaptcha/whyrecaptcha
*/

$use_captcha=false;
$publickey = ''; // Public Key
$privatekey = ''; // Private Key

/*
Registro de usuarios:
Para activar el registro colocar "$can_register = true;" y para desactivarlo, colocar "$can_register = false;"
*/

$can_register = false;

/*
Usuarios VIP:
Para activar el registro colocar "$can_vip = true;" y para desactivarlo, colocar "$can_vip = false;"
*/

$can_vip = false;

/*
Ocultar lista en inicio
*/

$hidelist = false;

/*
Confirma e-mail de registro de los usuarios
*/
$mailcheck = false;

/*
URL mode:
Los modos de url posibles: 0 para numérico (?v=123), 1 para alfabético (?v=abc) y 2 para modo compatibilidad (?v=123 -> ?v=abc)
*/
$uri_mode = 0;

/*******************************************************************************
ADVERTENCIA: El captcha y el password no pueden ser habilitados sumultaneamente
********************************************************************************/

if ($use_captcha && $use_password) {exit('Error: El captcha y el password no pueden ser habilitados sumult&aacute;neamente');}

if (empty($keylogmin)) { exit('Error: Auth Key no definida');}

?>
