<?php
$thefile="options-save.php";
require ("init.php");

if (!(isset($_POST['nc']) && checkNonce($_POST['nc'])))
  exit('Nonce inválido o expirado.');

if (!$is_admin) {
	@header("Location:logmin.php");
	exit('<meta http-equiv="Refresh" content="0;url=logmin.php">');
}

$user = $_POST['user'];
$pass = $_POST['pass'];
$passc = $_POST['passc'];
$key = $_POST['key'];
$name = $_POST['name'];
$desc = $_POST['desc'];
$register = $_POST['register'];
$vip = $_POST['vip'];
$passw = $_POST['passw'];
$captcha = $_POST['captcha'];
$public = $_POST['public'];
$private = $_POST['private'];
$abajo = $_POST['abajo'];
$arriba = $_POST['arriba'];
$home =$_POST['home'];
$hidelist = $_POST['hidelist'];
$uri_mode = $_POST['urlmode'];
$mailcheck = $_POST['mailcheck'];

if ($pass=='' && $passc==''){
	$pass=$adminpass;
}else{
	if ($pass!=$passc){ exit('Error: Las contrase&ntilde;as no coinciden'); }
}
if ($captcha=='true' && $passw=='true') {exit('Error: El captcha y el password no pueden ser habilitados sumult&aacute;neamente');}
if (empty($key)) { exit('Error: Auth Key no definida');}

$text = '<?php
ob_start();
$config[\'db_host\']=\''.$config['db_host'].'\'; // nombre del host
$config[\'db_name\']=\''.$config['db_name'].'\'; // nombre de la base de datos
$config[\'db_user\']=\''.$config['db_user'].'\'; // usuario de la BD
$config[\'db_pass\']=\''.$config['db_pass'].'\'; // password de la BD

// Datos del admin y del sitio

$adminuser = \''.$user.'\'; //nombre del administrador
$adminpass = \''.$pass.'\'; //password del administrador
$keylogmin = \''.$key.'\';      //Auth Key - no dejes en blanco esto ejemplo: 2hGsTH6Fs145sWhfs36HYDAAD66
$webname = \''.$name.'\'; // Nombre del sitio
$webdescription = \''.$desc.'\'; // Descripcion del sitio

/*
Configuracion de password:
colocar "$use_password=true;" para habilitar el uso de password
y "$use_password=false;" para desabilitarlo
*/
$use_password='.$passw.';

/*
Configuracion de captcha:
colocar "$use_captcha=true;" para habilitar el uso de password
y "$use_captcha=false;" para desabilitarlo.
La Public Key y la Private Key las consigues registr�ndote en http://www.google.com/recaptcha/whyrecaptcha
*/

$use_captcha='.$captcha.';
$publickey = \''.$public.'\'; // Public Key
$privatekey = \''.$private.'\'; // Private Key

/*
Registro de usuarios:
Para activar el registro colocar "$can_register = true;" y para desactivarlo, colocar "$can_register = false;"
*/

$can_register = '.$register.';

/*
Usuarios VIP:
Para activar el registro colocar "$can_vip = true;" y para desactivarlo, colocar "$can_vip = false;"
*/

$can_vip = '.$vip.';

/*
Ocultar lista en inicio
*/

$hidelist = '.$hidelist.';

/*
Confirma e-mail de registro de los usuarios
*/
$mailcheck = '.$mailcheck.';

/*
URL mode:
Los modos de url posibles: 0 para numérico (?v=123), 1 para alfabético (?v=abc) y 2 para modo compatibilidad (?v=123 -> ?v=abc)
*/
$uri_mode = '.$uri_mode.';

/*******************************************************************************
ADVERTENCIA: El captcha y el password no pueden ser habilitados sumultaneamente
********************************************************************************/

if ($use_captcha && $use_password) {exit(\'Error: El captcha y el password no pueden ser habilitados sumult&aacute;neamente\');}

if (empty($keylogmin)) { exit(\'Error: Auth Key no definida\');}

?>';


$fp = fopen('config.php', 'w');
fwrite($fp, $text);
fclose($fp);

$fad = fopen('home.txt', 'w');
fwrite($fad, $home);
fclose($fad);

$fad1 = fopen('arriba.txt', 'w');
fwrite($fad1, $arriba);
fclose($fad1);

$fad2 = fopen('abajo.txt', 'w');
fwrite($fad2, $abajo);
fclose($fad1);

@header("Location:logminpanel.php?action=config");
exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php?action=config">');
?>
