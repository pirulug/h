<?php
require_once(__DIR__ . '/ReCaptcha.php');
require_once(__DIR__ . '/RequestMethod.php');
require_once(__DIR__ . '/RequestParameters.php');
require_once(__DIR__ . '/Response.php');
require_once(__DIR__ . '/RequestMethod/Post.php');
require_once(__DIR__ . '/RequestMethod/Socket.php');
require_once(__DIR__ . '/RequestMethod/SocketPost.php');

// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = '6LcKGwgTAAAAAI1xXBoWhXrFeFg4A4hgP1cLAI0C';
$secret = '6LcKGwgTAAAAAHoLZRW_OL8vHs4OlULXZOvkzKeY';
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>reCAPTCHA Example</title>
        <link rel="shortcut icon" href="//www.gstatic.com/recaptcha/admin/favicon.ico" type="image/x-icon"/>
        <style type="text/css">
            body {
                margin: 1em 5em 0 5em;
                font-family: sans-serif;
            }
            fieldset {
                display: inline;
                padding: 1em;
            }
        </style>
    </head>
    <body>
        <h1>reCAPTCHA Example</h1>
    <?php
    if (isset($_POST['g-recaptcha-response'])):
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

    if ($resp->isSuccess()):
        ?>
        <h2>Success!</h2>
        <?php
    else:
        ?>
        <h2>Something went wrong</h2>
        <p>The following error was returned: <?php
            foreach ($resp->getErrorCodes() as $code) {
                echo '<tt>' , $code , '</tt> ';
            }
            ?></p>>
    <?php
    endif;
else:
// Add the g-recaptcha tag to the form you want to include the reCAPTCHA element
    ?>
    <p>Complete the reCAPTCHA then submit the form.</p>
    <form action="" method="post">
        <fieldset>
            <legend>An example form</legend>
            <p>Example input A: <input type="text" name="ex-a" value="foo"></p>
            <p>Example input B: <input type="text" name="ex-b" value="bar"></p>

            <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
            <script type="text/javascript"
                    src="https://www.google.com/recaptcha/api.js?hl=es-419">
            </script>
            <p><input type="submit" value="Submit" /></p>
        </fieldset>
    </form>
<?php endif; ?>
</body>
</html>
