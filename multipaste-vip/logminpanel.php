<?php
$thefile="logminpanel.php";
require_once("init.php");

//comprobamos que se haya logueado e usuario
if (!$is_admin) {
  @header("Location:logmin.php");
  exit('<meta http-equiv="Refresh" content="0;url=logmin.php">');
}

//optenemos la opcion a ejecutar
@$action = $_GET['action'];

if (!$can_vip){
  echo'
    <style type="text/css">
      .vipbbcodebuton{display:none;padding:none;}
    </style>
  ';
}

if (isset($_GET['v']) && is_numeric ($_GET['v'])){
  $id = $_GET['v'];
}

switch ($action) {
    case 'new':
				include ("header.php");
				echo '<div><form method=post action=new.php charset="UTF-8" style="margin: 0px;padding: 0px;background: none;border: none;">';
				echo'<h4>Título: <input name=Titulo type=text></input></h4><br>';
        echo '<ul class="tabs">';
        for ($n=1;$n<=6;$n++){
				      echo '<li><div href="#tab'.$n.'"><b id="TTab'.$n.'" style="opacity: 0.5;">Desactivado</b></div></li>';
				}
        echo '</ul>';
        echo '<div class="tab_container" style="padding: 7px;">';
				for ($n=1;$n<=6;$n++){
  				echo '<div id="tab'.$n.'" class="tab_content" style="border: none;">Pestaña '.$n.': <input name=Mname'.$n.' type=text onchange="TTab(\'TTab'.$n.'\', this.value)" onkeyup="this.onchange();" onpaste="this.onchange();" oninput="this.onchange();"></input><br />
  				<div id="Mirror'.$n.'" onmouseover="BBhover(\'Mirror'.$n.'\')"><textarea id="TMirror'.$n.'" name="Mirror'.$n.'" cols="60" rows="20"></textarea></div></div>';
				}
        if ($can_vip) {
					echo'<br><label style="width: 100px;padding-left:20px;"><strong>Tipo de paste:</strong></label>
						<select name="tipo" id="tipo">
							<option value="0">P&uacute;blico</option>
							<option value="1">Vip</option>
						</select>';
				}
				if ($use_password) echo '<br /><label style="width: 100px;padding-left:20px;"><strong>Contrase&ntilde;a:</strong></label><input name=pass type=text value=""></input>';
				echo '<input name="nc" type=hidden value="'.getNonce().'"></input>';
				echo '<br /><center><input type=submit Value="Crear Nuevo"></div></center></form></div>';
				break;
		case 'config':
				include ("header.php");
					echo '
						<form method=post action=options-save.php charset="UTF-8">
						<h2>Datos de Admin</h2>
						<label><strong>Usuario Admin:</strong></label> <input name=user type=text value="'.$adminuser.'"></input><br />
						<label><strong>Nuevo Password:</strong></label> <input name=pass type=password></input><br />
						<label><strong>Confirmar:</strong></label> <input name=passc type=password></input><br />
						<label><strong>Auth Key:</strong></label><input name=key type=text value="'.$keylogmin.'"></input><br />
						<br />
						<h2>Datos del sitio</h2>
						<label><strong>Nombre:</strong></label> <input name=name type=text value="'.$webname.'"></input><br />
						<label><strong>Descripci&oacute;n:</strong></label> <input name=desc type=text value="'.$webdescription.'"></input><br />
						<h2>Publicidad</h2>
						<strong>Publicidad Arriba:</strong><br />
						<textarea id="arriba" name="arriba" cols="60" rows="10">'.file_get_contents('arriba.txt').'</textarea><br />
						<strong>Publicidad Abajo:</strong><br />
						<textarea id="abajo" name="abajo" cols="60" rows="10">'.file_get_contents('abajo.txt').'</textarea><br />';
            if ($hidelist){
              echo '<strong>Publicidad Inicio (en lugar del listado):</strong><br />
  						<textarea id="home" name="home" cols="60" rows="10">'.file_get_contents('home.txt').'</textarea><br />';
            }
						echo '<h2>M&oacute;dulos</h2>
						<label><strong>Registro de usuarios:</strong></label> <select name="register" id="register">';
						if ($can_register){
							echo '
							<option value="true">Activado</option>
							<option value="false">Desactivado</option>';
						}else{
							echo '
							<option value="false">Desactivado</option>
							<option value="true">Activado</option>';
						}
            echo '</select><br />
						<label><strong>Confirmación de e-mail:</strong></label> <select name="mailcheck" id="mailcheck">';
						if ($mailcheck){
							echo '
							<option value="true">Activado</option>
							<option value="false">Desactivado</option>';
						}else{
							echo '
							<option value="false">Desactivado</option>
							<option value="true">Activado</option>';
						}
						echo '</select><br />
						<label><strong>Usuarios VIP:</strong></label> <select name="vip" id="vip">';
						if ($can_vip){
							echo '
							<option value="true">Activado</option>
							<option value="false">Desactivado</option>';
						}else{
							echo '
							<option value="false">Desactivado</option>
							<option value="true">Activado</option>';
						}
            echo '</select><br />
						<label><strong>Modo de urls:</strong></label> <select name="urlmode" id="urlmode">';
						if ($uri_mode == 2){
							echo '
							<option value="2">Modo Compatibilidad (?v=123 -> ?v=abc)</option>
							<option value="1">Modo Alfabético (?v=abc)</option>
              <option value="0">Modo Numérico (?v=123)</option>';
						}else if ($uri_mode){
              echo '
							<option value="1">Modo Alfabético (?v=abc)</option>
              <option value="0">Modo Numérico (?v=123)</option>
              <option value="2">Modo Compatibilidad (?v=123 -> ?v=abc)</option>';
						} else {
              echo '
              <option value="0">Modo Numérico (?v=123)</option>
              <option value="1">Modo Alfabético (?v=abc)</option>
              <option value="2">Modo Compatibilidad (?v=123 -> ?v=abc)</option>';
						}
            echo '</select><br />
						<label><strong>Ocultar listado en inicio:</strong></label> <select name="hidelist" id="hidelist">';
						if ($hidelist){
              echo '
							<option value="true">Activado</option>
							<option value="false">Desactivado</option>';
						} else {
              echo '
              <option value="false">Desactivado</option>
							<option value="true">Activado</option>';
						}
						echo '</select><br />
						<label><strong>Contrase&ntilde;a en paste:</strong></label> <select name="passw" id="passw">';
						if ($use_password){
							echo '
							<option value="true">Activado</option>
							<option value="false">Desactivado</option>';
						}else{
							echo '
							<option value="false">Desactivado</option>
							<option value="true">Activado</option>';
						}
						echo '</select><br />
						<label><strong>Captcha en paste:</strong></label> <select name="captcha" id="captcha">';
						if ($use_captcha){
							echo '
							<option value="true">Activado</option>
							<option value="false">Desactivado</option>';
						}else{
							echo '
							<option value="false">Desactivado</option>
							<option value="true">Activado</option>';
						}
            echo '</select><br />';
            if ($use_captcha){
  						echo '
                <h2>ReCaptcha</h2>
  							<label><strong>Clave del sitio:</strong></label> <input name=public type=text value="'.$publickey.'"></input><br />
  							<label><strong>Clave secreta:</strong></label> <input name=private type=text value="'.$privatekey.'"></input><br />
  							Nota: Puedes obtener tus claves de recaptcha desde <a href="https://www.google.com/recaptcha/">aqu&iacute;</a>.
  							';
            }
            echo '<input name="nc" type=hidden value="'.getNonce().'"></input>';
						echo '<br /><br /><input type=submit Value="Guardar todo"></form>';
				break;
		case 'edit':
				  include ("header.php");
          if (!isset($id)) { exit('Error: parámetro faltante'); }
				  $pedir = $mysqli->query("Select * From paste WHERE pasteID=".$id);
				  $fila = $pedir->fetch_assoc();
			    if (isset($fila['Titulo'])) {
					echo '<div><form method=post action=edit.php charset="UTF-8" style="margin: 0px;padding: 0px;background: none;border: none;">
					<input type=hidden name=v value="'.$id.'">';
					echo '<h4>Título: <input name=Titulo type=text value="'.$fila['Titulo'].'"></input></h4><br>';
          echo '<ul class="tabs">';
          for ($n=1;$n<=6;$n++){
            if ($fila["Mname$n"]==''){
              echo '<li><div href="#tab'.$n.'"><b id="TTab'.$n.'" style="opacity: 0.5;">Desactivado</b></div></li>';
            } else{
              echo '<li><div href="#tab'.$n.'"><b id="TTab'.$n.'" style="opacity: 1;">'.$fila["Mname$n"].'</b></div></li>';
            }
          }
          echo '</ul>';
          echo '<div class="tab_container" style="padding: 7px;">';
					for ($n=1;$n<=6;$n++){
						echo '<div id="tab'.$n.'" class="tab_content" style="border: none;">Pestaña '.$n.': <input name=Mname'.$n.' type=text value="'.$fila["Mname$n"].'" onchange="TTab(\'TTab'.$n.'\', this.value)" onkeyup="this.onchange();" onpaste="this.onchange();" oninput="this.onchange();"></input><br />
						<div id="Mirror'.$n.'" onmouseover="BBhover(\'Mirror'.$n.'\')"><textarea id="TMirror'.$n.'" name="Mirror'.$n.'" cols="60" rows="20">'.$fila["Mirror$n"].'</textarea><br /></div></div>';
					}
          if ($can_vip) {
            echo '<br /><label style="width: 100px;padding-left:20px;"><strong>Tipo de paste:</strong></label>
            <select name="tipo" id="tipo">';
            if ($fila['vip']){
              echo '
              <option value="1">Vip</option>
              <option value="0">P&uacute;blico</option>';
            }else{
              echo '
              <option value="0">P&uacute;blico</option>
              <option value="1">Vip</option>';
            }
            echo '</select>';
          }
          if ($use_password) echo '<br /><label style="width: 100px;padding-left:20px;"><strong>Contrase&ntilde;a:</strong></label><input name=pass type=text value="'.$fila['pass'].'"></input>';
            echo '<input name="nc" type=hidden value="'.getNonce().'"></input>';
            echo '<br /><center><input type=submit Value="Aplicar cambios"></div></form></div></center>';
  				}else{
  					echo "<center><strong>Error: El id \"".$id."\" no existe</strong>";
  				}
				break;
		  case 'solve':
  		    if (!isset($id)) { exit('Error: parámetro faltante'); }
  		    if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
            exit('Nonce inválido o expirado.');
  		    if (!$resultado = $mysqli->query("UPDATE paste SET reported=0, Mesrep='' WHERE pasteID='$id'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
  				@header("Location:logminpanel.php");
  				exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php">');
				break;
		  case 'delete':
  				if (!isset($id)) { exit('Error: parámetro faltante'); }
  				if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
            exit('Nonce inválido o expirado.');
          if (!$resultado = $mysqli->query("DELETE FROM paste WHERE pasteID='$id'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
  				@header("Location:logminpanel.php");
  				exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php">');
				break;
		  case 'del':
  				if (!isset($id)) { exit('Error: parámetro faltante'); }
  				if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
            exit('Nonce inválido o expirado.');
          if (!$resultado = $mysqli->query("DELETE FROM paste WHERE pasteID='$id'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
  				@header("Location:logminpanel.php?action=showall&p=".@$_GET['p']."&s=".@$_GET['s']."&filter=".@$_GET['filter']);
  				exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php?action=showall'.@$_GET['p'].'&s='.@$_GET['s'].'&filter='.@$_GET['filter'].'>');
				break;
	    case 'showall':
				include ("header.php");
				echo '<center>';
				require_once("pagination.php");
				@$s=preparestr(urldecode ($_GET['s']));
				if (@$_GET['filter']=='all'){
					if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",10")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
					echo "<h3>Todos los Pastes:</h3>";
					$searchat='all';
				} elseif (@$_GET['filter']=='Searchall'){
					echo "<h3>Todos los Pastes: '".$s."':</h3>";
					if (!$pedir = $mysqli->query("Select * From paste WHERE Titulo LIKE '%$s%' ORDER BY pasteID DESC LIMIT ".$inicio.",10")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
					$searchat='all';
				} elseif (@$_GET['filter']=='Search'){
					echo "<h3>Mis Pastes: '".$s."':</h3>";
					if (!$pedir = $mysqli->query("Select * From paste WHERE Titulo LIKE '%$s%' AND user_id = 0 OR user_id IS NUll ORDER BY pasteID DESC LIMIT ".$inicio.",10")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
					$searchat='';
				}else {
					if (!$pedir = $mysqli->query("Select * From paste WHERE user_id = 0 OR user_id IS NUll ORDER BY pasteID DESC LIMIT ".$inicio.",10")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
					echo "<h3>Mis Pastes:</h3>";
					$searchat='';
				}
				echo "<strong><a href=logminpanel.php?action=showall>Mis Pastes</a> | <a href=logminpanel.php?action=showall&filter=all>Mostrar Todos</a></strong>";
				echo '<strong><table><tr><td colspan=5>

					<form method="get" class="searchform">
						<fieldset>
							<input type="hidden" value="showall" name="action">
							Buscar: &nbsp; <input type="text" value="'.@$s.'" name="s" style="max-width: 220px;"><button type="submit" name="filter" value="Search'.$searchat.'"></button>

						</fieldset>
					</form></td><tr><td>Titulo</td><td colspan=2>Opciones</td>';
        if ($uri_mode){
          $func = 'b10tobstr';
        } else {
          $func = 'doNoThing';
        }
				while($fila = $pedir->fetch_assoc()) {
				 echo "</tr><td><a href=.?v=".$func($fila['pasteID']).">".$fila['Titulo']. "</a></td><td><a href=logminpanel.php?action=edit&v=".$fila['pasteID'].">Editar</a></td><td><a href=?action=del&v=".$fila['pasteID']."&p=".$pagina."&s=".@$s."&filter=".@$_GET['filter']."&nc=".getNonce().">Eliminar</a></td><tr>";
				}
				echo "</tr></table>";
        $ant=$pagina-1;
        $sig=$pagina+1;
        if (isset($_GET['p']) && $pagina>'1') {
          echo '<p><br /><a href=?p='.$ant.'&action=showall&s='.@$s.'&filter='.@$_GET['filter'].'>&#60;&#60;Anterior</a>';
          $inicio = ($pagina * 10);
          if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",1")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
          $fila = $pedir->fetch_assoc();
          if (isset($fila['Titulo'])){
            echo'   |  <a href=?p='.$sig.'&action=showall&s='.@$s.'&filter='.@$_GET['filter'].'>Siguiente&#62&#62</a></p>';
          }
				 } else {
          $inicio = ($pagina * 10);
          if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",1")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
          $fila = $pedir->fetch_assoc();
          if (isset($fila['Titulo'])){
            echo'<a href=?p='.$sig.'&action=showall&s='.@$s.'&filter='.@$_GET['filter'].'>Siguiente&#62&#62</a></p>';
          }
				}
				break;
		case 'ban':
				if (isset($_GET['uid']) && is_numeric($_GET['uid'])){
				  if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
            exit('Nonce inválido o expirado.');
          $Uid=$_GET['uid'];
          if (!$pedir = $mysqli->query("UPDATE user SET banned=1 WHERE userID='$Uid'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
				}
				@header("Location:logminpanel.php?action=users");
				exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php?action=users">');
				break;
		case 'activate':
        if (isset($_GET['uid']) && is_numeric($_GET['uid'])){
          if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
            exit('Nonce inválido o expirado.');
          $Uid=$_GET['uid'];
          if (!$pedir = $mysqli->query("UPDATE user SET banned=0 WHERE userID='$Uid'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
				}
				@header("Location:logminpanel.php?action=users");
				exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php?action=users">');
				break;
		case 'loginas':
        if (isset($_GET['uid']) && is_numeric($_GET['uid'])){
          if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
            exit('Nonce inválido o expirado.');
          $Uid=$_GET['uid'];
          if (!$pedir = $mysqli->query("SELECT * from user WHERE  userID='$Uid'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          } else {
            $user = $pedir->fetch_assoc();
  					setcookie('muser', $user['userID'], time()+3600);
  					setcookie('msession',$user['pass'],time()+3600);
  					setcookie('logminpaste', '', time()-10);
  					@header("Location:userpanel.php");
  					exit('<meta http-equiv="Refresh" content="0;url=userpanel.php">');
          }
				}
				break;
		Case 'users':
				include ("header.php");
				echo '<center>';
				require ("pagination.php");
				@$filter=$_GET['filter'];
				switch ($filter){
					case 'inactive':
						if (!$pedir = $mysqli->query("Select * From user WHERE banned=1 ORDER BY userID DESC LIMIT ".$inicio.",10")){
              echo 'Fallo al consultar la base de datos.<br>';
          	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
          	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
          	  exit;
            }
					break;
					case 'active':
						if (!$pedir = $mysqli->query("Select * From user WHERE banned!=1 ORDER BY userID DESC LIMIT ".$inicio.",10")){
              echo 'Fallo al consultar la base de datos.<br>';
          	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
          	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
          	  exit;
            }
					break;
					case 'Search':
						@$s=preparestr(urldecode ($_GET['s']));
						if (!$pedir = $mysqli->query("Select * From user WHERE user LIKE '%$s%' ORDER BY userID DESC LIMIT ".$inicio.",10")){
              echo 'Fallo al consultar la base de datos.<br>';
          	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
          	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
          	  exit;
            }
					break;
					default:
						if (!$pedir = $mysqli->query("Select * From user ORDER BY userID DESC LIMIT ".$inicio.",10")){
              echo 'Fallo al consultar la base de datos.<br>';
          	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
          	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
          	  exit;
            }
					break;
				}
				echo "<strong><table><tr><h3>Todos los usuarios:</h3>";
				echo "<strong><a href=logminpanel.php?action=users>Mostar todos</a> | <a href=logminpanel.php?action=users&filter=inactive>Mostrar No-activos</a> | <a href=logminpanel.php?action=users&filter=active>Mostrar Activos</a></strong> | <a href=logminpanel.php?action=voucher>Cupones premium</a></strong>";
				echo '<table><tr><td colspan=5>
					<form method="get" class="searchform" action="logminpanel.php">
						<fieldset>
							<input type="hidden" value="users" name="action">
							Buscar: &nbsp; <input type="text" value="" name="s" style="max-width: 220px;"><button type="submit" name="filter" value="Search"></button>
						</fieldset>
					</form></td>
				<tr><td>Nombre de usuario</td><td>Tipo</td><td colspan=3>Opciones</td>';
				while($fila = $pedir->fetch_assoc()) {
  				 echo "</tr><tr><td>".$fila['user'].'</td>';
  				 if ($fila['vip']){
  				       echo "<td>Vip</td>";
  				 } else {
  				       echo "<td>Free</td>";
  				 }
  				 if ($fila['banned']){
  				       echo "<td><a href=logminpanel.php?action=activate&uid=".$fila['userID']."&nc=".getNonce().">Activar Usuario</a></td>";
  				 } else {
  				       echo "<td><a href=logminpanel.php?action=ban&uid=".$fila['userID']."&nc=".getNonce().">Desactivar Usuario</a></td>";
  				 }
  				 echo "<td><a href=logminpanel.php?action=loginas&uid=".$fila['userID']."&nc=".getNonce().">Logear como ".$fila['user']."</a></td>";
				}
				echo "</tr></table>";
				$ant=$pagina-1;
			  $sig=$pagina+1;
        if (isset($_GET['p']) && $pagina>'1') {
				  echo '<p><br /><a href=?p='.$ant.'&action=users&s='.@$s.'&filter='.@$_GET['filter'].'>&#60;&#60;Anterior</a>';
          $inicio = ($pagina * 10);
          if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",1")){
            echo 'Fallo al consultar la base de datos.<br>';
            echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
            echo 'Error: ' . addslashes ($mysqli->error).'<br>';
            exit;
          }
          $fila = $pedir->fetch_assoc();
					if (isset($fila['Titulo'])){
						echo'   |  <a href=?p='.$sig.'&action=users&s='.@$s.'&filter='.@$_GET['filter'].'>Siguiente&#62&#62</a></p>';
					}
				} else {
					$inicio = ($pagina * 10);
					if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",1")){
            echo 'Fallo al consultar la base de datos.<br>';
            echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
            echo 'Error: ' . addslashes ($mysqli->error).'<br>';
            exit;
          }
					$fila = $pedir->fetch_assoc();
					if (isset($fila['Titulo'])){
						echo'<a href=?p='.$sig.'&action=users&s='.@$s.'&filter='.@$_GET['filter'].'>Siguiente&#62&#62</a></p>';
					}
				}
				echo '</center>';
				break;
		case 'voucher':
				include ("header.php");
				echo '<center>';
				if ($can_vip) {
					echo '
					<h3>Crear Voucher Vip</h3>
					<form method=post>
						Cantidad de d&iacute;as vip: &nbsp; <input type=text name=days></input><br />
						<input type=submit Value="Crear Voucher">
					</form>';
					@$days=$_POST['days'];
					if (is_numeric (@$days)){
						$voucher=md5(date('ymdhms').rand());
						$sql = "INSERT INTO voucher (voucher,days) VALUES ('$voucher','$days')";
						if (!$pedir = $mysqli->query($sql)){
							echo "Error al crear voucher";
						} else {
						echo "se ha creado el voucher: ".$voucher." de ".$days." d&iacute;as vip";
						}
					}
					if (!$pedir = $mysqli->query("Select * From voucher ORDER BY voucherID")){
            echo 'Fallo al consultar la base de datos.<br>';
            echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
            echo 'Error: ' . addslashes ($mysqli->error).'<br>';
            exit;
          }
					echo "<strong><table><tr><h3>Lista de Vouchers:</h3>";
					echo "<table><tr><td>Voucher</td><td>D&iacute;as</td><td>Opciones</td>";
					while($fila = $pedir->fetch_assoc()) {
  					echo "</tr><tr><td>".$fila['voucher'].'</td>';
  					echo "<td>".$fila['days']."</td>";
  					echo "<td><a href=\"logminpanel.php?action=delvoucher&v=".$fila['voucherID']."&nc=".getNonce()."\">Eliminar</a></td>";
					}
					echo "</tr></table>";
				} else { echo "El m&oacute;dulo vip esta desactivado";}
					echo '</center>';
				break;
		case 'delvoucher':
				if (!isset($id)) { exit('Error: parámetro faltante'); }
				if (!(isset($_GET['nc']) && checkNonce($_GET['nc'])))
          exit('Nonce inválido o expirado.');
        if (!$pedir = $mysqli->query("DELETE FROM voucher WHERE voucherID='$id'")){
          echo 'Fallo al consultar la base de datos.<br>';
          echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
          echo 'Error: ' . addslashes ($mysqli->error).'<br>';
          exit;
        }
				@header("Location:logminpanel.php?action=voucher");
				exit('<meta http-equiv="Refresh" content="0;url=logminpanel.php?action=voucher">');
				break;
		default:
				include ("header.php");
				echo '<center>';
		    require_once("pagination.php");
				if (@$_GET['filter']=='all'){
          if (!$pedir = $mysqli->query("Select * From paste WHERE reported=1 LIMIT ".$inicio.",10")){
            echo 'Fallo al consultar la base de datos.<br>';
            echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
            echo 'Error: ' . addslashes ($mysqli->error).'<br>';
            exit;
          }
					echo "<strong><h3>Todos los reportados:</h3>";
				} else {
          if (!$pedir = $mysqli->query("Select * From paste WHERE reported=1 and (user_id = 0 OR  user_id IS NUll) LIMIT ".$inicio.",10")){
            echo 'Fallo al consultar la base de datos.<br>';
            echo "Select * From paste WHERE reported=1 and (user_id = 0 OR  user_id IS NUll) LIMIT ".$inicio.",10<br>";
            echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
            echo 'Error: ' . addslashes ($mysqli->error).'<br>';
            exit;
          }
					echo "<strong><table><tr><h3>Mis Pastes reportados:</h3>";
				}
				echo "<strong><a href=logminpanel.php>Mis Pastes Reportados</a> | <a href=logminpanel.php?filter=all>Mostrar Todos</a></strong>";
				echo "<table><tr><td>Titulo</td><td>Razon del reporte</td><td colspan=3>Opciones</td>";
        if ($uri_mode){
          $func = 'b10tobstr';
        } else {
          $func = 'doNoThing';
        }
				while($fila =$pedir->fetch_assoc()) {
				 echo "</tr><td><a href=.?v=".$func($fila['pasteID']).">".$fila['Titulo']. "</a></td><td>".nl2br($fila['Mesrep'])."</input></td><td><a href=logminpanel.php?action=edit&v=".$fila['pasteID'].">Editar</a></td><td><a href=?action=solve&v=".$fila['pasteID']."&nc=".getNonce().">Solucionado</a></td>";
				}
				echo "</tr></table>";
				$ant=$pagina-1;
			  $sig=$pagina+1;
				if (isset($_GET['p']) && $pagina>'1') {
          echo '<p><br /><a href=?p='.$ant.'&s='.@$s.'&filter='.@$_GET['filter'].'>&#60;&#60;Anterior</a>';
          $inicio = ($pagina * 10);
          if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",1")){
            echo 'Fallo al consultar la base de datos.<br>';
            echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
            echo 'Error: ' . addslashes ($mysqli->error).'<br>';
            exit;
          }
          $fila = $pedir->fetch_assoc();
				 if (isset($fila['Titulo'])){
				  echo'   |  <a href=?p='.$sig.'&s='.@$s.'&filter='.@$_GET['filter'].'>Siguiente&#62&#62</a></p>';
				 }
				} else {
				 $inicio = ($pagina * 10) + 1;
         if (!$pedir = $mysqli->query("Select * From paste ORDER BY pasteID DESC LIMIT ".$inicio.",1")){
           echo 'Fallo al consultar la base de datos.<br>';
           echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
           echo 'Error: ' . addslashes ($mysqli->error).'<br>';
           exit;
         }
         $fila = $pedir->fetch_assoc();
				 if (isset($fila['Titulo'])){
				 		echo'<a href=?p='.$sig.'&s='.@$s.'&filter='.@$_GET['filter'].'>Siguiente&#62&#62</a></p>';
				}
				}
				echo '</strong>';
				break;
		}
echo '</center>';
include("footer.php");
?>
