<?php
//Incluyo la configuración
require_once("config.php");
require_once("functions.php");

// Comprobando carga correcta
if (@$thefile=="")  exit('Acceso denegado');

//creo la conexión
$mysqli = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']);

//compruebo la conexión
if ($mysqli->connect_errno){
  echo 'Fallo al conectarse a MySQL.<br>';
  echo 'Errno: ' . addslashes ($mysqli->connect_errno).'<br>';
  echo 'Error: ' . addslashes ($mysqli->connect_error).'<br>';
  exit;
}

 /*********************/
// Cargando variables //
/*********************/

// ID del paste
if (isset($_GET['v'])){
  if ($thefile!="index.php"){
    is_numeric($_GET['v']) &&  $id = $_GET['v'];
  } else{
    if ($uri_mode == 2){
      if (is_numeric($_GET['v'])){
        $aux = b10tobstr($_GET['v']);
        @header("Location:.?v=".$aux);
        exit('<meta http-equiv="Refresh" content="0;url=.?v='.$aux.'">');
      } else  {
        $aux = bstrtob10($_GET['v']);
        $aux && $id = $aux;
      }
    } else if ($uri_mode){
      $aux = bstrtob10($_GET['v']);
      $aux && $id = $aux;
    } else{
      is_numeric($_GET['v']) &&  $id = $_GET['v'];
    }
  }
}

@$userinfo = userdata(); // Informacion del usuario

// Definiendo variable Is_admin
if (isset($_COOKIE["logminpaste"]) && $_COOKIE["logminpaste"]==md5($adminuser.$keylogmin.$adminpass)) {
	$is_admin=true;
} else {
	$is_admin=false;
}

// Definiendo variable Owner y Fila
$owner=false;
if (@$thefile=="index.php") {
	if (isset($id)){
	  $pedir = $mysqli->query("Select * From paste WHERE pasteID=".$id);
	  $fila = $pedir->fetch_assoc();
		if (isset($fila['Titulo'])) {
		  // comprobando propiedad
		  $Uid=@$userinfo['userID'];
		  if (@$userinfo['userID'] != '') {
			    if (!$r = $mysqli->query("SELECT * from paste WHERE pasteID='$id' AND user_id = '$Uid'")){
            echo 'Fallo al consultar la base de datos.<br>';
        	  echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        	  echo 'Error: ' . addslashes ($mysqli->error).'<br>';
        	  exit;
          }
			    if ($r->num_rows > 0) {
				        $owner=true;
			    }
      }
		}
	}
}

// desactivamo el captcha y password si es admin o dueño del paste para facilitar edición
if ($thefile=="index.php" && ($is_admin || $owner)) {
	$use_captcha=false;
	$use_password=false;
}

?>
