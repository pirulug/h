<?php

/*Preparar texto*/
function preparestr($string)
{
    global $mysqli;
    $string = trim($mysqli->real_escape_string($string));

    $string = str_replace(
        array("š", "º", "#","·","&", "¡","¿", "Ž",">", "<", "<"),
        array('&uml;', '&ordm;', '#','&middot;','&amp;', '&iexcl;','&iquest;', '&acute;','&gt;', '&lt;'),
        $string
    );

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('&aacute;', '&agrave;', '&auml;', '&acirc;', '&ordf;', '&Aacute;', '&Agrave;', '&Acirc;', '&Auml;'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('&eacute;', '&egrave;', '&euml;', '&ecirc;', '&Eacute;', '&Egrave;', '&Ecirc;', '&Euml;'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
         array('&iacute;', '&igrave;', '&iuml;', '&icirc;', '&Iacute;', '&Igrave;', '&Iuml;', '&Icirc;'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('&oacute;', '&ograve;', '&ouml;', '&ocirc;', '&Oacute;', '&Ograve;', '&Ouml;', '&Ocirc;'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('&uacute;', '&ugrave;', '&uuml;', '&ucirc;', '&Uacute;', '&Ugrave;', '&Ucirc;', '&Uuml;'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('&ntilde;', '&Ntilde;', '&ccedil;', '&Ccedil;'),
        $string
    );

    return $string;
}

/* hex */

function strToHex($string)
{
    $hex='';
    for ($i=0; $i < strlen($string); $i++)
    {
        $hex .= '\x'.dechex(ord($string[$i]));
    }
    return $hex;
}

/* bbcodes */
function bb_parse($string) {
  global $userinfo, $is_admin, $owner, $id, $can_vip;

  // BBcodes - General
  $bbcodes = array();
  $bbcodes[0] = '/\[center\](.+?)\[\/center\]/si';
  $bbcodes[1] = '/\[b\](.+?)\[\/b\]/si';
  $bbcodes[2] = '/\[i\](.+?)\[\/i\]/si';
  $bbcodes[3] = '/\[size=([0-9]+px)\](.+?)\[\/size\]/si';
  $bbcodes[4] = '/\[color=([a-zA-Z0-9\#]+)\](.+?)\[\/color\]/si';
  $bbcodes[5] = '/\[quote\](.+?)\[\/quote\]/si';
  $bbcodes[6] = '/\[url=(https?:\/\/[a-zA-Z0-9%=&;:\_\/\+\-\!\#\?\.\(\)]+)\](.+?)\[\/url\]/si';
  $bbcodes[7] = '/\[img\](https?:\/\/[a-zA-Z0-9%=&;:\_\/\+\-\!\#\?\.\(\)]+)\[\/img\]/si';
  $bbcodes[8] = '/\[youtube\](https?:\/\/(www.)?youtube.com)?\/(v\/|watch\?v\=|watch\?feature\=player_embedded\&v\=|watch\?feature\=player_detailpage\&v\=)([-|~_0-9A-Za-z]+)&?.*?\[\/youtube\]/i';
  $bbcodes[9] = '/\[vip\](.+?)\[\/vip\]/si';

  $reemplazos = array();
  $reemplazos[0] = '<center>$1</center>';
  $reemplazos[1] = '<strong>$1</strong>';
  $reemplazos[2] = '<em>$1</em>';
  $reemplazos[3] = '<span style="font-size:$1">$2</span>';
  $reemplazos[4] = '<span style="color:$1">$2</span>';
  $reemplazos[5] = '<blockquote>$1</blockquote>';
  $reemplazos[6] = '<a href="$1">$2</a>';
  $reemplazos[7] = '<img src="$1"></img>';
  $reemplazos[8] = '<iframe width="600" height="400" src="https://www.youtube.com/embed/$4" frameborder="0" allowfullscreen></iframe>';
  $reemplazos[9] = '$1';

  // añadido especial - bbcode VIP
  if (@!$userinfo['vip'] && !$is_admin && !$owner && $can_vip){
    $reemplazos[9] = '<div class="vip"><img draggable="false" src="https://s.w.org/images/core/emoji/2/svg/1f512.svg" width=50 height=50> <strong>Contenido oulto:</strong> Para ver este contenido <a href="login.php?return='.$id.'">Inicia Sesión</a> en tu cuenta VIP.</div>';
  }

  $string = preg_replace($bbcodes, $reemplazos, $string);

  // BBcodes especiales (con callback)
  $string = preg_replace_callback(
                                  '/\[code\](.+?)\[\/code\]/si',
                                  function ($match){
                                    return "<pre>".str_replace(array("\r\n", "\r"),"",nl2br($match[1]))."</pre>";
                                  },
                                  $string
                                );
  $string = preg_replace_callback(
                                  '/\[jd\](.+?)\[\/jd\]/si',
                                  function ($match){
                                    return '<form method="post" target="hidden" class="jd" action="http://127.0.0.1:9666/flash/add"><INPUT TYPE="hidden" NAME=urls VALUE="'.base64_encode($match[1]).'"><INPUT TYPE="SUBMIT" NAME="submit" VALUE="Clic para a&ntilde;adir a JDownloader" class="jdbtn"></form>';
                                  },
                                  $string
                                );

  // Auto-enlazar urls sin romper los bbcodes
  $string = str_replace('="http','="url', $string);
  $string = preg_replace('#(https?://[A-z0-9./\-\?\%\&\=\+\!\#]+)#', '<a target="_blank" href="$1">$0</a>',$string);
  $string = str_replace('="url','="http', $string);

  return $string;
}

/* User datos */
function userdata(){
  global $mysqli;
  $data= array();
  @$pass = $_COOKIE['msession'];
  @$Uid = $_COOKIE['muser'];
  if (@$pass!='' and @$Uid!='') {
    if (!$r = $mysqli->query("SELECT * from user WHERE pass = '$pass' AND userID = '$Uid'")){
      echo 'Fallo al consultar la base de datos.<br>';
      echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
      echo 'Error: ' . addslashes ($mysqli->error).'<br>';
    }
    if ($r->num_rows > 0) {
      $data = $r->fetch_assoc();
    } else {
      exit ('Error fatal en codigo de autentificacion');
    }

    if ($data['banned']) {
      exit ('Error: Su cuenta se encuentra desactivada');
    }
  }

  if (@$data['vip']) {
    $restdays=dateDiff(date('d-m-Y'),$data['vipdate']);
    if ($restdays<=0) {
      $sql = "UPDATE user SET vip=0, vipdate='' WHERE userID='$Uid'";
      if (!$r = $mysqli->query($sql)){
        echo 'Fallo al consultar la base de datos.<br>';
        echo 'Errno: ' . addslashes ($mysqli->errno).'<br>';
        echo 'Error: ' . addslashes ($mysqli->error).'<br>';
      }
  }
}

return @$data;
}

function dateDiff($start, $end) {
  $start_ts = strtotime($start);
  $end_ts = strtotime($end);
  $diff = $end_ts - $start_ts;
  return round($diff / 86400);
}

function get_userid(){
  global $userinfo;
  if (@$userinfo['userID']=='') {
    setcookie('muser', '', time()-1);
    @header("Location:login.php");
    exit('<meta http-equiv="Refresh" content="0;url=login.php">');
  } else {
    return $userinfo['userID'];
  }
}

function b10tobstr($b10){
  $base = 'ijklGHLMcsFqraPQRtuvwVWXYZmnopIJdeNOyzABfgCDESTUKxbh';
  $strlen = strlen($base);
  $bstr = '';
  while ($b10>0) {
    $mod = $b10 % $strlen;
    $b10 = (int)($b10 / $strlen);
    $bstr = $base[$mod].$bstr;
  }
  return $bstr;
}

function bstrtob10($str){
  $base = 'ijklGHLMcsFqraPQRtuvwVWXYZmnopIJdeNOyzABfgCDESTUKxbh';
  $strlen = strlen($base);
  $i = strlen($str)-1;
  $number = 0;
  $pos = 0;
  while ($i>=0) {
    $aux = $str[$i];
    $aux2 = strpos($base,$aux);
    if (is_numeric($aux2)){
      $number = $number + ($aux2 * pow($strlen,$pos));
    } else {
      $i = 0;
      $number = false;
    }
    $pos++;
    $i--;
  }
  return $number;
}

function doNoThing($anything){
  return $anything;
}

function getNonce(){
  global $keylogmin, $is_admin;
  if ($is_admin) {
    $user_id = 0;
  } else {
    $user_id = get_userid();
  }
  $str = time();
  return bin2hex(openssl_encrypt($str, 'aes-128-ecb', $keylogmin.$user_id, OPENSSL_RAW_DATA));
}

function checkNonce($nonce = ''){
  global $keylogmin, $is_admin;;
  if ($is_admin) {
    $user_id = 0;
  } else {
    $user_id = get_userid();
  }
  $result = openssl_decrypt(hex2bin($nonce), 'aes-128-ecb', $keylogmin.$user_id, OPENSSL_RAW_DATA);
  if ($result > (time()- 1800))
    return true;
  return false;
}
