<?php require_once ("init.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"[]>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
if ($thefile=="index.php" && isset($fila['Titulo'])) {
  echo '<title>'.$fila['Titulo'].' | '.$webname.'</title>';
} else {
  echo '<title>'.$webname.'</title>';
}
?>
<link rel="stylesheet" type="text/css" href="tabs.css">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts.js"></script>
</head>

<body id="wordpress-org">
	<div class="header">
		<div>
			<p class="title"><a href="index.php" name="top"><?php echo $webname; ?></a></p>
			<p class="tagline"><?php echo $webdescription; ?></p>
		</div>
	</div>
<div class="nav">
		<div>
			<ul class="menu">
				<li class="page_item"><a href="index.php">Inicio</a></li>
<?php
if ($is_admin) {
echo '<li class="page_item"><a href=logminpanel.php?action=new>Nuevo</a></li>
<li class="page_item"><a href=logminpanel.php>Reportados</a></li>
<li class="page_item"><a href=logminpanel.php?action=showall>Pastes</a></li>
<li class="page_item"><a href=logminpanel.php?action=users>Usuarios</a></li>
<li class="page_item"><a href=logminpanel.php?action=config>Configuraciones</a></li>
<li class="page_item"><a href=logoutmin.php>Cerrar sesi&oacute;n</a></li>';
} elseif (@$userinfo['userID']!='') {
echo '<li class="page_item"><a href=userpanel.php?action=new>Nuevo</a></li>
<li class="page_item"><a href=userpanel.php>Reportados</a></li>
<li class="page_item"><a href=userpanel.php?action=showall>Mis Pastes</a></li>
<li class="page_item"><a href=usercf.php>Configuraci&oacute;n</a></li>
<li class="page_item"><a href=logout.php>Cerrar sesi&oacute;n</a></li>';
} else {
echo '<li class="page_item"><a href=login.php>Login</a></li>
<li class="page_item"><a href=register.php>Registrarse</a></li>';
}
?>
			</ul>
			<div class="clear"><!-- --></div>
		</div>
</div>
<?php if (@$userinfo['userID']!='') echo '<div class="welcomeuser">Sesi&oacute;n iniciada como <strong>'.$userinfo['user'].'</strong></div>';?>
	<div class="content">
